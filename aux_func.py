# Purpose:  Contains library of auxiliary functions to interface with the C++ code provided by Navis

def get_image_list(zip_path):
    """
    :param zip_path: the path to the images zip file
    :return: image_list: the list of images path inside zip_path
    """

    import os
    import zipfile
    import pandas as pd

    image_list = []
    subfolder = []
    img_base = []

    if os.path.exists(zip_path):
        zip_handle = zipfile.ZipFile(zip_path, 'r')
        zipfile_list = zip_handle.namelist()

        for image_path in zipfile_list:
            if image_path.endswith('.jpg') or image_path.endswith('.bmp') or image_path.endswith('.png'):
                image_path_full = os.path.join(zip_path, image_path)
                image_path_full = image_path_full.replace('/', '\\')
                image_list.append(image_path_full)

                image_name_split = image_path_full.split('\\')
                sub_append = image_name_split[-2]
                base_append = image_name_split[-1].replace('.jpg', '')
                base_append = base_append.replace('.bmp', '')
                base_append = base_append.replace('.png', '')

                subfolder.append(sub_append)
                img_base.append(base_append)

        images_count = len(image_list)
        print(f"\nThe zip file path, {zip_path}, contains {images_count} images\n")

    else:
        print('\n[Error] Zip file path does not exist\n')

    df = pd.DataFrame({'img path': image_list, 'subfolder': subfolder, 'base': img_base})
    df['subfolder'] = df['subfolder'].astype(int)
    df['base'] = df['base'].astype(int)

    df = df.sort_values(['subfolder', 'base'])

    new_img_list = df['img path'].tolist()

    return new_img_list
