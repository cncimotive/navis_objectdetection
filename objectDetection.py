"""
Purpose:  Road Object Detection, Contains library of functions to interface with the C++ code provided by Navis
           1. Accepts the path to zipped image folder from C++ code
           2. Lists valid targets for forward pass
           3. Executes forward pass on extracted images
           4. Passes output bounding boxes and detected object classes back to C++ code

Version 2.0 (16/6/2020)
The following changes were made to run_pred_zip_obj:
1. Added DEBUG MODE to draw post-processing relevant info
2. Reduce the amount of loop level for readibility
3. Added new post-processing named 'combine overlap'
4. Any bounding boxes that had been edited with combine overlap
   will be colored orange during DEBUG MODE = True

predict_by_batch v1.0 (18/8/2020):
Pass  image paths and the code processes all the images by batch.
Specify the number of images to process per batch in 'batch_size'.
For 2080ti and gpu usage 0.2, passing 5 image path is recommended.
Requirement:
1. darkflow libary with change to flow.py (function return_predict2)
2. timer.py (for timing processes)
These are the changes in comparison to previous run_pred_zip_obj:
1. Multithread for loading image batch
2. Prediction process on all image batch
3. Multithread on drawing images

predict_by_double (21/8/2020):
Pass image paths and the code process all the images using two neural network.
First, two neural network will be loaded using load_net_obj2.
Then, the images will be passed using multithread.
For 2080ti and gpu usage 0.2, passing 2 image path is recommended.
Requirement:
1. darkflow libary with change to flow.py (function return_predict2)

predict_by_batch v1.1 (25/8/2020):
1. Added ability to read and extract straight from zipfile
- Get the path of the image inside the zipfile using the text file created from function checknlog_zipfile
in aux_func.py

predict_by_batch v1.2 (10/9/2020):
1. Added NPI calculation. If image fails the NPI, the object result would be return as NPI values.
2. Simplified the error handling for images that fail to load.
3. Remove logging if image fail to load

predict_by_batch v1.3 (1/10/2020):
Change procsel divider from 5 to 4, thus gpu usage will be 0.25 instead of 0.2
Bug fixes:
1. Remove drawing npi images
2. If crop overlap occured, the 'crop' field will have a value of None
3. Error image will return Error: -1 instead of Error: 1

predict_by_batch v1.4 (7/10/2020):
Added Deformation limit
Bug fixes:
1. Car Intersection is now performed after full image detection and not crop detections

predict_by_batch v1.5(13/10/2020):
Small object filter can now be filtered based on box width and height
Box deformation and Car overlap is now done at the ending part of post-processing and
when the detections is for full image.

predict_by_batch v1.6(29/12/2020):
Added post-process filter roadmark near bridge

predict_by_batch v1.7(4/1/2021):
Added Multiple Object Filter and change subcategories to object categories

predict_by_batch v1.8(4/2/2021):
Added gatebar, supplement and camera custom confidence limit

predict_by_batch v2.0(3/3/2021):
Replaced the custom confidence limit with confidence factor

"""
import os
import cv2
import numpy as np
import concurrent.futures
from zipfile import ZipFile
from shapely.geometry import Polygon

# Initialize threshold
CONF_THRESHOLD = 0

# TODO
DEBUG_MODE = False
# Post-Process parameters
BOX_AREA = False  # If true, use box area. If false, use width and height
# For box area
BOX_MIN_LIMIT_SIGN = 400  # For signboard
BOX_MIN_LIMIT_ROAD = 400  # For Roadmark
BOX_MIN_LIMIT_REST = 250  # For the rest of the objects
# For width and height [width, height]
WH_LIMIT_PROB = [15, 15]  # Prohibition
WH_LIMIT_INST = [15, 15]  # Instruction
WH_LIMIT_CAUT = [20, 20]  # Caution
WH_LIMIT_CAM = [15, 15]  # Camera
WH_LIMIT_TL = [15, 15]  # Traffic light
WH_LIMIT_GB = [20, 20]  # Gatebar
WH_LIMIT_ROAD = [20, 8]  # Roadmark
WH_LIMIT_SUPP = [15, 10]  # Supplement
WH_LIMIT_SIGN = [20, 15]  # Signboard
WH_LIMIT_SE = [15, 20]  # Speed enforcement
WH_LIMIT_SB = [20, 20]  # Speed bump
# Filter detections at car
FILTER_BOX_AT_CAR = False
FILTER_CAR_INTERSECTION = 0.6
# Pixel value limit - For NPI
# Pixel too bright limit
UPLIMIT_BRIGHT = 255
LOWLIMIT_BRIGHT = 240
# Pixel too dark limit
UPLIMIT_DARK = 15
LOWLIMIT_DARK = 0
# Percentage of dark and bright pixel - For NPI
# Values that are higher than the limit will be categorised as Fail
BRIGHT_PERCENT_LIMIT = 30
DARK_PERCENT_LIMIT = 30
# Deformation limit
DEFORMATION_FILTER = True
DEFORMATION_INTERSECTION = 0.5
DEFORMATION_Y_LIMIT = 400
# Roadmark filter if near bridge
BRIDGE_FILTER_STAT = False
BRIDGE_IOU = 0.5
# Filter out bridge predictions
BRIDGE_REMOVE = False
# # Multi Objects Filter
# MULT_OBJ_FILTER_STAT = True
# MULT_OBJ_IOU = 0.5
# Confidence level factor
CAMERA_FACTOR = 1.6
SUPPLEMENT_FACTOR = 1.8
GATEBAR_FACTOR = 1.2
ROADMARK_FACTOR = 1.2
SPEEDBUMP_FACTOR = 1.6
INSTRUCTION_FACTOR = 1.4


# loading network
def load_net_obj(procsel, threshold):
    """
    2/10/2020
    Update the proc divider from 5 to 4. THus gpu usage will be 0.25
    Access and modifies the global CONF_THRESHOLD.
    """

    from darkflow.net.build import TFNet
    import os

    # Changing global variable
    global CONF_THRESHOLD

    CONF_THRESHOLD = float(threshold)

    # Check confidence threshold
    if CONF_THRESHOLD == 0:
        print('WARNING: Confidence threshold was not set')
        print('Setting confidence threshold to default 0.25')
        CONF_THRESHOLD = 0.25

    print(f'Confidence Threshold: {CONF_THRESHOLD}')
    proc = int(procsel)
    print('proc:', proc)

    options = {
        'pbLoad': 'built_graph-OD-v12.0/NAVIS-ObjectDetection.pb',
        'metaLoad': 'built_graph-OD-v12.0/NAVIS-ObjectDetection.meta',
        'threshold': threshold,
        'gpu': proc/4
    }

    tfnet = TFNet(options)

    print("\n[*] Net loaded")

    if os.path.isdir('output'):

        print('\n[*] Output directory exists: ./output')

    else:

        print('\n[*] Output directory does not exist. Creating output folder: ./output')
        os.mkdir('output')

    return tfnet


def midpoint(point1, point2):
    """
    Calculate midpoint from two points

    :param point1: point 1
    :param point2: point 2
    :return:
        value of midpoint in integer
    """
    return int((point1 + point2)/2)


def distance(x1, y1, x2, y2):
    """
    Calculate distance between two coordinates

    :param x1: point x1
    :param y1: point y1
    :param x2: point x2
    :param y2: point y2
    :return dist:
        returns distance value
    """
    import math
    dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    return dist


def find_inrange(left_list, right_list, prediction_list):
    """
    From all the left detection, find the right side detection
    that is within the range of left detection box

    :param left_list:
        Left side detection list
    :param right_list:
        Right side detection list
    :param prediction_list:
        The overall prediction results from the neural network.
        Will not be modified here.
    :return left_box_match:
        List of left box matches with right box. The format is
        [[left id, [all right id matches], [...], [...]]]
    """

    left_box_match = []  # Store return list

    # From left side detection, to append id of right side detection if
    # the midpoint of the box is within the left side box range
    for each_left in left_list:
        # Store any right side detection which satisfy the condition
        right_inrange_left = []

        max_y_range = prediction_list[each_left].get('topleft')['y']
        min_y_range = prediction_list[each_left].get('bottomright')['y']

        for each_right in right_list:
            max_y = prediction_list[each_right].get('topleft')['y']
            min_y = prediction_list[each_right].get('bottomright')['y']

            mid_y = midpoint(max_y, min_y)

            if min_y_range >= mid_y >= max_y_range:
                right_inrange_left.append(each_right)
            else:
                continue

        left_box_match.append([each_left, right_inrange_left])

        # Remove any pair without a match
        left_box_match = [x for x in left_box_match if x[1]]

    return left_box_match


def create_new_box(id_left, id_right, prediction_list):
    """
    Prepare the new bounding box dictionary

    :param id_left:
        The selected box id on left side
    :param id_right:
        The selected box id on right side
    :param prediction_list:
        The overall prediction results from the neural network.
        Will not be modified here
    :return new_box_dict:
        New detection box dictionary. The 'edit' will return True
    """
    # select the average confidence
    confidence_list = [prediction_list[id_left].get('confidence'),
                       prediction_list[id_right].get('confidence')]
    confidence_val = sum(confidence_list) / len(confidence_list)

    # Prep new coordinates
    top_x_list = [prediction_list[id_left].get('topleft')['x'],
                  prediction_list[id_right].get('topleft')['x']]
    topleft_x = min(top_x_list)
    top_y_list = [prediction_list[id_left].get('topleft')['y'],
                  prediction_list[id_right].get('topleft')['y']]
    topleft_y = min(top_y_list)
    bot_x_list = [prediction_list[id_left].get('bottomright')['x'],
                  prediction_list[id_right].get('bottomright')['x']]
    botright_x = max(bot_x_list)
    bot_y_list = [prediction_list[id_left].get('bottomright')['y'],
                  prediction_list[id_right].get('bottomright')['y']]
    botright_y = max(bot_y_list)

    # Label prep
    label_type = prediction_list[id_left].get('label')

    # Create new detection box dictionary
    new_box_dict = {'bottomright': {'x': botright_x, 'y': botright_y},
                    'confidence': confidence_val,
                    'label': label_type,
                    'topleft': {'x': topleft_x, 'y': topleft_y}, 'crop': None,
                    'edit': True}
    return new_box_dict


def right_closest(left_index, list_right_index, prediction_list):
    """
    Find the right detection id that is closest
    to the left detection. The comparison points will be
    the midpoints of the detections. Also returns the distance
    for all the right detections.

    :param left_index:
        Selected left detection box index
    :param list_right_index:
        List of right detection box index
    :param prediction_list:
        The overall prediction results from the neural network.
        Will not be modified here

    :return right_index_closest:
        Index of right detection index that is closest to
        the selected left index
    """
    # Coordinates of midpoint left detection
    x_left = prediction_list[left_index].get('bottomright')['x']
    y_left = midpoint(prediction_list[left_index].get('bottomright')['y'],
                      prediction_list[left_index].get('topleft')['y'])

    # Get the distance for between the left detection and all the right detections
    box_distance = [distance(x_left, y_left,
                             prediction_list[each_right].get('topleft')['x'],
                             midpoint(prediction_list[each_right].get('bottomright')['y'],
                                      prediction_list[each_right].get('topleft')['y']))
                    for each_right in list_right_index]

    # Select the right detection index closest to the left detection
    right_index_closest = list_right_index[box_distance.index(min(box_distance))]

    return right_index_closest


def color_box(detection_box):
    """
    Determine the color of the text and bounding box based on the
    conditions
    """
    if detection_box['edit']:
        if DEBUG_MODE:
            color = (0, 128, 255)
        else:
            color = (0, 255, 0)
    else:
        color = (0, 255, 0)
    return color


def process_overlap_crop(pred_list: list, delete_list: list, leftside_list: list, rightside_list: list):
    """
    To make sure that all the boxes on the left side to have matches to the right side.
    If the conditions are suitable, a new detection box will created.
    The bounding box that was selected to create the new bounding box will has it ID stored
    to the deletion list.

    Suitable conditions for a new detection box:
    1. The labels for both right and left box must be similar
    2. Midpoint of right box is within the y-axis of left box
    3. The midpoint of right box is closest to the left box

    :param pred_list: The overall prediction results from the neural network. Will not be modified here
    :param delete_list: The list containing ID of the boxes to be deleted. The ID is added by extending the list
    :param leftside_list: Contains the ID of boxes from the left side of the crop overlay
    :param rightside_list: Containts the ID of boxes from the right side of the crop overlay

    :return output_box: List of new detection boxes. May return empty list
    :return delete_list: Returns back the deletion list where new ID to be deleted had been
                        extended. May return an unchanged deletion list.
    """
    output_box = []  # Store the new detection box dictionary

    # Get the unique labels on the left side first
    left_labels = [pred_list[x].get('label') for x in leftside_list]
    unique_labels = list(set(left_labels))

    for each_label in unique_labels:
        # Filter ids based on the labels
        left_match_label = [x for x in leftside_list if pred_list[x].get('label') == each_label]
        right_match_label = [x for x in rightside_list if pred_list[x].get('label') == each_label]

        # Skip if there are any sides that have no boxes
        if not left_match_label or not right_match_label:
            continue

        # Find right box detection that is within the left box range
        left_right_match = find_inrange(left_match_label, right_match_label, pred_list)

        # Skip to next label if there are no matching left and right detection
        if not left_right_match:
            continue

        # If there is only one matching pair, to store the
        # new bounding box dictionary and skip to next label
        if len(left_right_match) == 1:
            # Check if there is only one right side match
            if len(left_right_match[0][1]) == 1:
                selected_left = left_right_match[0][0]
                selected_right = left_right_match[0][1][0]
                new_detection_box = create_new_box(selected_left, selected_right, pred_list)

                output_box.append(new_detection_box)
                delete_list.extend([selected_left, selected_right])

            # If there are multiple, to choose the one closest to the left side
            else:
                selected_left = left_right_match[0][0]
                selected_right_list = left_right_match[0][1]
                selected_right = right_closest(selected_left, selected_right_list, pred_list)
                new_detection_box = create_new_box(selected_left, selected_right, pred_list)

                output_box.append(new_detection_box)
                delete_list.extend([selected_left, selected_right])

            continue

        # Process for multiple matching pairs.
        # To first get the distance of all right detections based on the left detections.
        # The distance is calculated by getting the midpoint
        # between y-axis and the edge of the box for the x-coordinate
        all_distance_list = []  # [[left id, right id, distance], [..., ..., ...], ...]
        for each_match in left_right_match:
            left_index = each_match[0]

            distance_list = [[left_index, each_right, distance(pred_list[left_index].get('bottomright')['x'],
                                                               midpoint(pred_list[left_index].get('bottomright')['y'],
                                                                        pred_list[left_index].get('topleft')['y']),
                                                               pred_list[each_right].get('topleft')['x'],
                                                               midpoint(pred_list[each_right].get('bottomright')['y'],
                                                                        pred_list[each_right].get('topleft')['y']))]
                             for each_right in each_match[1]]

            all_distance_list.extend(distance_list)

        # Sort the list where the lowest distance the the first index
        all_distance_list = sorted(all_distance_list, key=lambda x: x[2])

        # Remove the left id if it is seen twice
        left_index_distance = [x[0] for x in all_distance_list]
        unique_left = list(set(left_index_distance))

        filtered_left = []
        while True:
            if not unique_left:
                break

            for each_distance in all_distance_list:
                if each_distance[0] in unique_left:
                    filtered_left.append(each_distance)
                    unique_left.remove(each_distance[0])

        # Remove the right id if it is seen twice
        right_index_distance = [x[1] for x in filtered_left]
        unique_right = list(set(right_index_distance))

        filtered_right = []
        while True:
            if not unique_right:
                break

            for each_distance in filtered_left:
                if each_distance[1] in unique_right:
                    filtered_right.append(each_distance)
                    unique_right.remove(each_distance[1])

        # Create new bounding box for all the filtered list and store id to deletion list
        for each_match in filtered_right:
            selected_left = each_match[0]
            selected_right = each_match[1]
            new_detection_box = create_new_box(selected_left, selected_right, pred_list)

            output_box.append(new_detection_box)
            delete_list.extend([selected_left, selected_right])

    return output_box, delete_list


def split_chunks(list_to_process, n):
    """
    Yield n-sized chunks from list
    Parameters
    ----------
    list_to_process: A list to be split to chunks
    n: number of elements in each chunk

    Returns
    -------
    Yield each chunks with n amount of element
    """
    # For item i in a range that is a length of l,
    for i in range(0, len(list_to_process), n):
        # Create an index range for l of n items:
        yield list_to_process[i:i+n]


def crop_img_list(img_list, x, y, h):
    """
    Crop list of images into three crops for single panoramic image. This is part of
    pre-processing to feed images to the neural network.
    Parameters
    ----------
    img_list: list of panoramic images
    x: crop constant
    y: crop constant
    h: crop constant

    Returns
    -------
    A flatten list of cropped panoramic images.

    """
    all_crops = []
    for each_img in img_list:
        crop1 = each_img[y:y + h, x:x + h]
        crop2 = each_img[y:y + h, x + h:x + h * 2]
        crop3 = each_img[y:y + h, x + h * 2:x + h * 3]
        crops = [crop1, crop2, crop3]
        all_crops.append(crops)

    # flatten the cropped images
    flat_crop = [item for sublist in all_crops for item in sublist]

    return flat_crop


def calc_box_area(box_prediction):
    """
    Calculate area of bounding box
    :param box_prediction: Dictionary of prediction
    :return: area of box
    """
    box_area = abs(box_prediction['topleft']['x'] - box_prediction['bottomright']['x']) * abs(
        box_prediction['topleft']['y'] - box_prediction['bottomright']['y'])
    return box_area


def calc_box_wh(box_prediction):
    """
    Calculate width and height of bounding box
    :param box_prediction: Dictionary of prediction
    :return:
    width_box: Width of box
    height_box: Height of box
    """
    width_box = abs(box_prediction['topleft']['x'] - box_prediction['bottomright']['x'])
    height_box = abs(box_prediction['topleft']['y'] - box_prediction['bottomright']['y'])

    return width_box, height_box


def process_each_result(three_crops_result, front_car_shape,
                        x, h, crop_overlap_region1, crop_overlap_region2, img_width, conf_limit, img_path):
    """
    Post-processing of neural network prediction for a single panoramic image.
    Post-processing includes:
    1. Filter out small bounding box sizes
        a. Signboard and road mark have their own size limit
    2. Remove prediction at the car area
    3. Crop overlap
        a. match bounding boxes belonging to different crops from the same panoramic image
    4. Remove detection within Deformation limit
    5. Remove 'roadmark' within 'bridge' detection
    6. Convert subcategories to object categories
    7. Multi objects filter - Report on multiple detections of different objects
    with low confidence threshold
    Parameters
    ----------
    three_crops_result: a list of dictionaries for 3 crops of a single panoramic image
    front_car_shape: List of front car shape coordinates
    x: crop constant
    h: crop constant
    crop_overlap_region1: info on region 1
    crop_overlap_region2: info on region 1
    img_width: image width
    img_path: image path
    conf_limit: value of confidence threshold

    Returns
    -------
    A dictionary within a list containing the result for a single panoramic image

    """
    # Store result
    results_1 = []
    result_2 = []
    results = []

    # Prep for confidence threshold value specific to obj
    camera_conf = conf_limit * CAMERA_FACTOR
    suppl_conf = conf_limit * SUPPLEMENT_FACTOR
    gateb_conf = conf_limit * GATEBAR_FACTOR
    roadm_conf = conf_limit * ROADMARK_FACTOR
    speedbump_conf = conf_limit * SPEEDBUMP_FACTOR
    inst_conf = conf_limit * INSTRUCTION_FACTOR

    for i, result_crop in enumerate(three_crops_result):
        for each_prediction in result_crop:
            # TODO
            # Filter out small bounding box sizes and confidence limit
            # Signboard and road mark has its own size limit
            if BOX_AREA:
                obj_conf = float(each_prediction['confidence'])
                if each_prediction['label'] == 'road mark':
                    if calc_box_area(each_prediction) < BOX_MIN_LIMIT_ROAD or obj_conf < roadm_conf:
                        continue
                elif each_prediction['label'] == 'signboard':
                    if calc_box_area(each_prediction) < BOX_MIN_LIMIT_SIGN:
                        continue
                else:
                    area_compare = calc_box_area(each_prediction)
                    if area_compare < BOX_MIN_LIMIT_REST:
                        continue
            else:
                obj_conf = float(each_prediction['confidence'])

                if each_prediction['label'] == 'road mark':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_ROAD[0] or height_compare < WH_LIMIT_ROAD[1] or obj_conf < roadm_conf:
                        continue
                elif each_prediction['label'] == 'signboard':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_SIGN[0] or height_compare < WH_LIMIT_SIGN[1]:
                        continue
                elif each_prediction['label'] == 'prohibition':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_PROB[0] or height_compare < WH_LIMIT_PROB[1]:
                        continue
                elif each_prediction['label'] == 'instruction':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_INST[0] or height_compare < WH_LIMIT_INST[1] or obj_conf < inst_conf:
                        continue
                elif each_prediction['label'] == 'caution':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_CAUT[0] or height_compare < WH_LIMIT_CAUT[1]:
                        continue
                elif each_prediction['label'] == 'camera':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_CAM[0] or height_compare < WH_LIMIT_CAM[1] or obj_conf < camera_conf:
                        continue
                elif each_prediction['label'] == 'traffic light':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_TL[0] or height_compare < WH_LIMIT_TL[1]:
                        continue
                elif each_prediction['label'] == 'gate bar':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_GB[0] or height_compare < WH_LIMIT_GB[1] or obj_conf < gateb_conf:
                        continue
                elif each_prediction['label'] == 'supplement':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_SUPP[0] or height_compare < WH_LIMIT_SUPP[1] or obj_conf < suppl_conf:
                        continue
                elif each_prediction['label'] == 'speed enforcement':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_SE[0] or height_compare < WH_LIMIT_SE[1]:
                        continue
                elif each_prediction['label'] == 'speed bump':
                    width_compare, height_compare = calc_box_wh(each_prediction)
                    if width_compare < WH_LIMIT_SB[0] or height_compare < WH_LIMIT_SB[1] or obj_conf < speedbump_conf:
                        continue
                else:
                    pass

            # Change the crop bounding box to match the full original panoramic image
            # The changes depends on the crop number iteration
            if i == 0:
                each_prediction['topleft']['y'] = each_prediction['topleft']['y'] + 100
                each_prediction['bottomright']['y'] = each_prediction['bottomright']['y'] + 100

            if i == 1:
                each_prediction['topleft']['x'] = each_prediction['topleft']['x'] + x + h
                each_prediction['topleft']['y'] = each_prediction['topleft']['y'] + 100

                each_prediction['bottomright']['x'] = each_prediction['bottomright']['x'] + x + h
                each_prediction['bottomright']['y'] = each_prediction['bottomright']['y'] + 100

            if i == 2:
                each_prediction['topleft']['x'] = each_prediction['topleft']['x'] + x + h * 2
                each_prediction['topleft']['y'] = each_prediction['topleft']['y'] + 100

                each_prediction['bottomright']['x'] = each_prediction['bottomright']['x'] + x + h * 2
                each_prediction['bottomright']['y'] = each_prediction['bottomright']['y'] + 100

            # Add new dict element to indicate the prediction belongs to which crop
            each_prediction['crop'] = i

            # Add new dict element to indicate whether the prediction has been edited or not
            each_prediction['edit'] = False

            result_temp = [each_prediction]
            results_1.extend(result_temp)

        # Crop Overlap post-processing
        # If the image is divided to three corps, any detection box that overlaps at the edge of the
        # cropped area will be combined

        # Group the detections to the crop overlap list if the edge
        # of the prediction box is within the crop overlap region
        left_side_region1 = [result_id for result_id, each_result in enumerate(results_1) if
                             each_result.get('crop') == 0 and
                             crop_overlap_region1[0] <= each_result.get('bottomright')['x'] <= crop_overlap_region1[1]]

        right_side_region1 = [result_id for result_id, each_result in enumerate(results_1) if
                              each_result.get('crop') == 1 and
                              crop_overlap_region1[0] <= each_result.get('topleft')['x'] <= crop_overlap_region1[1]]

        left_side_region2 = [result_id for result_id, each_result in enumerate(results_1) if
                             each_result.get('crop') == 1 and
                             crop_overlap_region2[0] <= each_result.get('bottomright')['x'] <= crop_overlap_region2[1]]

        right_side_region2 = [result_id for result_id, each_result in enumerate(results_1) if
                              each_result.get('crop') == 2 and
                              crop_overlap_region2[0] <= each_result.get('topleft')['x'] <= crop_overlap_region2[1]]

        # List to delete modified detection
        box_to_delete = []

        # List to hold new detection box created from crop overlap process
        new_box_list = []

        # Process for crop overlap region 1
        if left_side_region1 and right_side_region1:
            new_box, box_to_delete = process_overlap_crop(results_1, box_to_delete,
                                                          left_side_region1, right_side_region1)
            if new_box:
                new_box_list.extend(new_box)

        # Process for crop overlap region 2
        if left_side_region2 and right_side_region2:
            new_box, box_to_delete = process_overlap_crop(results_1, box_to_delete,
                                                          left_side_region2, right_side_region2)
            if new_box:
                new_box_list.extend(new_box)

        # Remove detections based on the id stored in box_to_delete
        results_1 = [each_result for i, each_result in enumerate(results_1) if i not in box_to_delete]

        # Add the newly created bounding box
        results_1.extend(new_box_list)

    # Find bridge prediction
    bridge_list = []
    if BRIDGE_FILTER_STAT:
        bridge_list = [i for i in results_1 if i['label'] == 'bridge']

    # Loop over the results of a full image
    for each_prediction in results_1:
        # Filter out Bounding Box within the car shape
        if front_car_shape:
            tl = (each_prediction['topleft']['x'], each_prediction['topleft']['y'])
            br = (each_prediction['bottomright']['x'], each_prediction['bottomright']['y'])

            # Added coordinates for calculating intersection percentage
            bl = (tl[0], br[1])
            tr = (br[0], tl[1])

            # Constructing polygon for bounding box and car shape
            bbox_polygon = Polygon([br, tr, tl, bl])
            shape_front_polygon = Polygon(front_car_shape)

            # Intersection between bounding box and car shape
            intersection1 = bbox_polygon.intersection(shape_front_polygon)

            # Percentage of Intersection
            percentage_intersection1 = (intersection1.area / bbox_polygon.area)

            # Conditioning for drawing bounding box based on the intersection

            if percentage_intersection1 > FILTER_CAR_INTERSECTION:
                continue

        # Deformation limit
        if DEFORMATION_FILTER:
            # Coordinates of bounding box
            tl = (each_prediction['topleft']['x'], each_prediction['topleft']['y'])
            br = (each_prediction['bottomright']['x'], each_prediction['bottomright']['y'])

            # Added bounding box coordinates for calculating intersection percentage
            bl = (tl[0], br[1])
            tr = (br[0], tl[1])

            # offset for deformation to avoid error
            offset_width = int(img_width - 1)

            list_deform_coord = [(offset_width, DEFORMATION_Y_LIMIT),
                                 (offset_width, 0), (0, 0), (0, DEFORMATION_Y_LIMIT)]

            # Constructing polygon for deformation and bounding box
            bbox_polygon = Polygon([br, tr, tl, bl])
            shape_deform = Polygon(list_deform_coord)

            # Intersection between bounding box and deformation limit
            intersection1 = bbox_polygon.intersection(shape_deform)

            # Percentage of Intersection
            percentage_intersection1 = (intersection1.area / bbox_polygon.area)

            if percentage_intersection1 > DEFORMATION_INTERSECTION:
                continue

        bridge_cond = 1
        if BRIDGE_FILTER_STAT:
            # Filter roadmark if bridge exists and it is currently roadmark prediction
            if each_prediction['label'] == 'roadmark' and bridge_list:
                bridge_cond = bridge_filter(each_prediction, bridge_list)
        if bridge_cond == 0:
            continue

        # Filter out any bridge predictions
        if BRIDGE_REMOVE:
            if each_prediction['label'] == 'bridge':
                continue

        # Change the object subcategories to object categories
        if each_prediction['label'] == 'brown_sign':
            each_prediction['label'] = 'signboard'
        elif each_prediction['label'] == 'supp_cat1':
            each_prediction['label'] = 'supplement'
        elif each_prediction['label'] == 'supp_cat2':
            each_prediction['label'] = 'supplement'
        elif each_prediction['label'] == 'supp_cat3':
            each_prediction['label'] = 'supplement'

        result_2.append(each_prediction)

    # for each_prediction in result_2:
    #     # Multi Object filter
    #     mult_cond = 1
    #     if MULT_OBJ_FILTER_STAT:
    #         mult_cond = mult_obj_filter(each_prediction, result_2, img_path)
    #
    #     # # Uncomment if want to remove multiple object detection from log
    #     # if mult_cond == 0:
    #     #     continue

        results.append(each_prediction)

    return results


# def mult_obj_filter(each_prediction, all_prediction, img_path_full):
#     """
#     Report any detection with multiple object detection and low threshold. In the report, the compared
#     object will be labelled with 'C_'
#     :param each_prediction: Neural network prediction of an object
#     :param all_prediction: All neural network predictions from the single image
#     :param img_path_full: image path
#     :return:
#         return int 0 if filtering required. return int 1 if not required.
#         However for now it is only for reporting
#     """
#     filter_condition = False
#
#     # Remove current prediction from perdiction list
#     filter_all_prediction1 = [i for i in all_prediction if i != each_prediction]
#
#     # Filter off objects with same current object category
#     filter_all_prediction2 = [i for i in filter_all_prediction1 if i['label'] != each_prediction['label']]
#
#     for each_compare in filter_all_prediction2:
#         # Coordinates for comparison
#         tl_c = (each_compare['topleft']['x'], each_compare['topleft']['y'])
#         br_c = (each_compare['bottomright']['x'], each_compare['bottomright']['y'])
#         bl_c = (tl_c[0], br_c[1])
#         tr_c = (br_c[0], tl_c[1])
#
#         # Coordinates for current object
#         tl_p = (each_prediction['topleft']['x'], each_prediction['topleft']['y'])
#         br_p = (each_prediction['bottomright']['x'], each_prediction['bottomright']['y'])
#         bl_p = (tl_p[0], br_p[1])
#         tr_p = (br_p[0], tl_p[1])
#
#         # Constructing polygon
#         bbox_c = Polygon([br_c, tr_c, tl_c, bl_c])
#         bbox_p = Polygon([br_p, tr_p, tl_p, bl_p])
#
#         # Intersection between bounding boxes
#         polygon_intersection = bbox_c.intersection(bbox_p).area
#
#         # IOU
#         polygon_union = bbox_c.union(bbox_p).area
#         iou_value = polygon_intersection / polygon_union
#
#         if iou_value > MULT_OBJ_IOU:
#             # If condition achived, to filter if object has lower threshold
#             if each_prediction['confidence'] < each_compare['confidence']:
#                 # Report on log
#                 img_path = img_path_full.split('/')
#                 zip_path = img_path[1].replace('.zip', '')
#                 sub_path = img_path[2]
#                 img_path = img_path[3].replace('.jpg', '')
#                 obj_confidence = each_prediction['confidence']
#                 obj_label = each_prediction['label']
#                 topleftx = each_prediction['topleft']['x']
#                 toplefty = each_prediction['topleft']['y']
#                 bottomrx = each_prediction['bottomright']['x']
#                 bottomry = each_prediction['bottomright']['y']
#
#                 # Info on comparison
#                 c_obj_confidence = each_compare['confidence']
#                 c_obj_label = each_compare['label']
#                 c_topleftx = each_compare['topleft']['x']
#                 c_toplefty = each_compare['topleft']['y']
#                 c_bottomrx = each_compare['bottomright']['x']
#                 c_bottomry = each_compare['bottomright']['y']
#
#                 mult_out_dir = 'output'
#                 os.makedirs(mult_out_dir, exist_ok=True)
#
#                 mult_log_path = f'{mult_out_dir}/{zip_path}_mult_obj.txt'
#
#                 # Name the columns in the log first if log does not exists
#                 if not os.path.exists(mult_log_path):
#                     with open(mult_log_path, "w") as output:
#                         row_text = f'Zip file, Subfolder, Image, Object Label, Top Left X, ' \
#                                    f'Top Left Y, Bottom Right X, Bottom Right Y, Confidence, C_Object Label, ' \
#                                    f'C_Top Left X, C_Top Left Y, C_Bottom Right X, C_Bottom Right Y, C_Confidence\n'
#                         output.write(row_text)
#
#                 with open(mult_log_path, 'a') as output:
#                     row_text = f'{zip_path}, {sub_path}, {img_path}, {obj_label}, {topleftx}, {toplefty}' \
#                                f', {bottomrx}, {bottomry}, {obj_confidence}, {c_obj_label}, {c_topleftx}, ' \
#                                f'{c_toplefty}, {c_bottomrx}, {c_bottomry}, {c_obj_confidence}\n'
#                     output.write(row_text)
#
#                 # Indicate new filter condition
#                 filter_condition = True
#                 # Break loop if already required to filter off
#                 break
#
#     if filter_condition:
#         return 0
#     else:
#         return 1


def bridge_filter(each_prediction, bridge_list):
    """
    Filter out roadmark if near bridge.
    :param each_prediction: Neural network prediction of an object
    :param bridge_list: List of neural network prediction of bridge
    :return:
        return int 0 if filtering required. return int 1 if not required
    """
    filter_condition = False
    for each_bridge in bridge_list:
        # Coordinates for bridge
        tl_b = (each_bridge['topleft']['x'], each_bridge['topleft']['y'])
        br_b = (each_bridge['bottomright']['x'], each_bridge['bottomright']['y'])
        bl_b = (tl_b[0], br_b[1])
        tr_b = (br_b[0], tl_b[1])

        # Coordinates for roadmark
        tl_r = (each_prediction['topleft']['x'], each_prediction['topleft']['y'])
        br_r = (each_prediction['bottomright']['x'], each_prediction['bottomright']['y'])
        bl_r = (tl_r[0], br_r[1])
        tr_r = (br_r[0], tl_r[1])

        # Constructing polygon for bridge and roadmark
        bbox_bridge = Polygon([br_b, tr_b, tl_b, bl_b])
        bbox_road = Polygon([br_r, tr_r, tl_r, bl_r])

        # Intersection between bounding boxes
        polygon_intersection = bbox_road.intersection(bbox_bridge).area

        # IOU
        polygon_union = bbox_road.union(bbox_bridge).area
        iou_value = polygon_intersection / polygon_union

        if iou_value > BRIDGE_IOU:
            filter_condition = True
            # Break loop if already required to be filtered off
            break

    if filter_condition:
        return 0
    else:
        return 1


def draw_image_output(each_process):
    """
    If the image is not zipped, the function will draw based on the original image path.
    Else, if the image is zipped, the output will be in the 'output' folder
    If debug mode is true, additional info will be drawn to the
    images.
    For multithreading.
    Parameters
    ----------
    each_process: List containing info for drawing a single image

    Returns
    -------
    No return
    """
    # img_result, image, img_path, debug, car_shape, crop_overlap_region1, crop_overlap_region2
    img_result = each_process[0]
    image = each_process[1]
    img_dir_path = each_process[2]
    debug = each_process[3]
    car_shape_front = each_process[4]
    crop_overlap_region1 = each_process[5]
    crop_overlap_region2 = each_process[6]

    if '.zip' in img_dir_path:
        # Output folder
        out_folder = 'output'

        # Get the basename of the zipped folder and its subfolder
        zip_base = [a for a in img_dir_path.split('/') if '.zip' in a][0]
        zip_base = zip_base.replace('.zip', '')
        no_zipbase = img_dir_path.split('.zip/', 1)[1]
        zip_subfolder, img_basename = no_zipbase.rsplit('/', 1)

        # Create the file directory if not exist
        folder_path = f'{out_folder}/{zip_base}/{zip_subfolder}'
        os.makedirs(folder_path, exist_ok=True)

        # Rename the image directory path
        img_dir_path = f'{folder_path}/{img_basename}'

    for result in img_result:
        if result['confidence'] > 0.05:
            tl = (result['topleft']['x'], result['topleft']['y'])
            br = (result['bottomright']['x'], result['bottomright']['y'])
            label = result['label']
            confidence = result['confidence']
            text = '{}: {:.0f}%'.format(label, confidence * 100)
            # frame = cv2.polylines(frame,[pts],True, (0,255,255)) #Draw the car shape

            # Get the appropriate color for the bounding box situation
            color_type = color_box(result)

            image = cv2.rectangle(image, tl, br, color_type, 1)
            image = cv2.putText(image, text, tl, cv2.FONT_HERSHEY_COMPLEX, 1, color_type, 3)

    if debug:
        # Car shape
        if car_shape_front:
            shape1_c1 = car_shape_front[0]
            shape1_c2 = car_shape_front[1]
            shape1_c3 = car_shape_front[2]
            shape1_c4 = car_shape_front[3]

            pts1 = np.array([[shape1_c1[0], shape1_c1[1]],
                            [shape1_c2[0], shape1_c2[1]],
                            [shape1_c3[0], shape1_c3[1]],
                            [shape1_c4[0], shape1_c4[1]]])
            pts1 = pts1.reshape((-1, 1, 2))
            cv2.polylines(image, [pts1], True, (0, 0, 255), 3)

        # Crop combine detection
        image = cv2.putText(image, 'Debug Mode', (50, 50), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 255), 3)

        cv2.line(image, (1365, 0), (1365, 2048), (0, 255, 0), thickness=2)

        cv2.line(image, (crop_overlap_region1[0], 0), (crop_overlap_region1[0], 2048), (255, 0, 0), thickness=2)
        cv2.line(image, (crop_overlap_region1[1], 0), (crop_overlap_region1[1], 2048), (255, 0, 0), thickness=2)

        cv2.line(image, (1365 * 2, 0), (1365 * 2, 2048), (0, 255, 0), thickness=2)

        cv2.line(image, (crop_overlap_region2[0], 0), (crop_overlap_region2[0], 2048), (255, 0, 0), thickness=2)
        cv2.line(image, (crop_overlap_region2[1], 0), (crop_overlap_region2[1], 2048), (255, 0, 0), thickness=2)

    cv2.imwrite(img_dir_path, image)


def load_image(image_path):
    """
    Load images from image path.
    If the path is a zipfile, the function will decode the image first.
    For multithreading.
    Error handling (return the image path):
    1) Check file existence if path is not zipfile
    2) Ensure image loaded is not empty for both zip and non-zip

    Parameters
    ----------
    image_path: Directory of single panoramic image

    Returns
    -------
    Array of single panoramic image
    String False if image cannot be loaded
    """
    # Check if the image path is a zipfile
    if '.zip' in image_path:
        zip_path, img_in_zip = image_path.split('.zip/', 1)
        zip_path = zip_path + '.zip'

        # Decode image from zipfile
        with ZipFile(zip_path) as zip_file:
            data = zip_file.read(img_in_zip)
            try:
                loaded_img = cv2.imdecode(np.frombuffer(data, np.uint8), 1)
            except cv2.error:
                return 'False'
    else:
        # Check if image path exists
        if not os.path.exists(image_path):
            return 'False'
        loaded_img = cv2.imread(image_path)

    if loaded_img is None:
        return 'False'

    return loaded_img


def percentage(arr1):
    """
    Calculate percentage of element with value 255
    Returns
    -------
    Float of percentage value
    """
    height, width = arr1.shape[0], arr1.shape[1]
    total_element = height * width
    color_item = (arr1 == 255).sum()
    percent = color_item / total_element * 100

    return percent


def npi_img(image_array: np.ndarray):
    """
    Calculate the NPI of the image.
    Image will be cropped first where the top part is 400 pixel reduced.
    Afterwards, the image is converted to grayscale.
    A pixel is considered too bright if it is within the range of LOWLIMIT_BRIGHT and UPLIMIT_BRIGHT.
    Vice versa for pixel considered too dark.
    If the image has a value higher than either BRIGHT_PERCENT_LIMIT or DARK_PERCENT_LIMIT,
    the image will be categorised as failed/ npi image.
    Parameters
    ----------
    image_array: array of image

    Returns
    -------
    Boolean True if image pass the NPI and False if image fail the NPI
    """
    img = np.copy(image_array)

    y_top = 400  # Following the latest limit for object KPI
    y_bot = 1465
    x = 4095

    # Cut another 300pixel height from the top of image
    # -1 for avoiding index error
    img = img[y_top:y_bot, 0:x]

    # Convert color to gray
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Creating masks based on gray pixel value
    mask_bright = cv2.inRange(img_gray, LOWLIMIT_BRIGHT, UPLIMIT_BRIGHT)
    mask_dark = cv2.inRange(img_gray, LOWLIMIT_DARK, UPLIMIT_DARK)

    # Calculating percentages
    percent_bright = percentage(mask_bright)
    percent_dark = percentage(mask_dark)

    # Determine whether the image pass or fail
    if percent_bright < BRIGHT_PERCENT_LIMIT and percent_dark < DARK_PERCENT_LIMIT:
        npi_status = True
    else:
        npi_status = False

    return [npi_status, percent_bright, percent_dark]


def predict_by_batch(tfnet, img_path_list, points, draw_output=False):
    """
    Pass image paths and the code processes all the images by batch.
    If the images are corrupt, the object category will return Error: -1.

    Sidenote:
    list a - all of the initial image
    list b - undergo corrupt image check
    list c -undergo npi check

    Parameters
    ----------
    tfnet: neural network that will perform prediction
    img_path_list: List containing images to process
    points: Center of vehicle coordinates
    draw_output: If True, output bounding box drawing to original image paths

    Returns
    -------
    List of prediction results from the list of images.
    Example of return for a batch of 5 images with the first image having details:
    [{'img path: '01.jpg', 'obj': [{pred1}, {pred2}]}, {image2}, {image3}, {image4}, {image5}]
    pred1 example:
    {'label': 'road mark', 'confidence': 0.5246441,
    'topleft': {'x': 928, 'y': 1045}, 'bottomright': {'x': 1177, 'y': 1095}, 'crop': 0, 'edit': False}
    
    Example of return if the first image is corrupted:
    [{'img path: '01.jpg', 'obj': [{'Error': -1}]}, {image2}, {image3}, {image4}, {image5}]
    Example of return if the first image fails the NPI test:
    [{'img path: '01.jpg', 'obj': [{NPI': -1, 'Bright Percent': '35.0', 'Dark Percent': '25.0',
    'Bright Value': '255-240', 'Dark Value': '15-0'}]}, {image2}, {image3}, {image4}, {image5}]
    """
    # Initialize object confidence threshold
    obj_confd = CONF_THRESHOLD

    # Center of vehicle
    front_x, front_y, rear_x, rear_y = points[0][0], points[0][1], points[1][0], points[1][1]

    # constants for cropping
    x_crop = 0
    y_crop = 100
    h_crop = 1365  # offset

    # Define the crop overlap region based on x-axis
    crop_region1 = (1265, 1465)
    crop_region2 = (2630, 2830)

    # Ensure the image path is in correct format
    img_path_list = [a.replace('\\', '/') for a in img_path_list]
    img_path_list = [a.replace('//', '/') for a in img_path_list]

    # Load images for each batch
    # with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        # Load the images using multithreading
        # Loaded images are in (list a)
        img_list_a = list(executor.map(load_image, img_path_list))

        # Store list of index for corrupted images
        corrupt_index = [i for i, e in enumerate(img_list_a) if isinstance(e, str)]
        non_corrupt_index = [i for i, e in enumerate(img_list_a) if not isinstance(e, str)]
        # List of loaded images where corrupted images are removed (list b)
        img_list_b = [a for a in img_list_a if not isinstance(a, str)]
        # List of image path where corrupted images are removed(list b)
        img_path_b = [img_path_list[int(e)] for e in non_corrupt_index]
        corrupt_path = [img_path_list[int(e)] for e in corrupt_index]

        # In the event that there are only corrupted images, to skip checking npi
        if img_list_b:
            # Check NPI on image list b
            npi_status = list(executor.map(npi_img, img_list_b))
            # Store index of npi images based on list b
            fail_npi_index = [i for i, e in enumerate(npi_status) if not e[0]]
            pass_npi_index = [i for i, e in enumerate(npi_status) if e[0]]
            # Filter images of list b where npi has pass (list c)
            img_list_c = [img_list_b[i] for i in pass_npi_index]
            # Filter image paths of list b based on npi status(list c)
            img_path_c = [img_path_b[i] for i in pass_npi_index]
            fail_npi_path = [img_path_b[i] for i in fail_npi_index]
            # Get status of failed npi
            fail_npi_status = [npi_status[i] for i in fail_npi_index]
        else:
            fail_npi_index = []
            img_list_c = []
            img_path_c = []
            fail_npi_path = []

        # In the event that there are no viable images, to skip prediction process altogether
        if img_list_c:
            # Get height and width of an image
            height, width, _ = img_list_c[0].shape

            # Prepare coordinates of car to avoid object detection on top of the car
            if FILTER_BOX_AT_CAR:
                shape1_c1 = (front_x - 350, front_y - 20)
                shape1_c2 = (front_x - 1150, height)
                shape1_c3 = (front_x + 1150, height)
                shape1_c4 = (front_x + 350, front_y - 20)

                car_front_shape_list = [shape1_c1, shape1_c2, shape1_c3, shape1_c4]
            else:
                car_front_shape_list = []

            # Create a flat list of cropped images
            # This cropped images will be passed to the neural network
            flatten_img_list = crop_img_list(img_list_c, x_crop, y_crop, h_crop)

            # Run the neural network prediction on the cropped flatten image list
            results_temp_list = tfnet.return_predict2(flatten_img_list)

            # Group back the results according to an image
            result_img_crop_list = list(split_chunks(results_temp_list, 3))

            # Process for neural network results and group them back into complete result for each image
            result_img_list = []
            for each_img_result, each_path in zip(result_img_crop_list, img_path_c):
                # Preliminary post-processing for each crop and includes crop overlap
                # The three crops will be grouped to one result image
                processed_img_result = process_each_result(each_img_result, car_front_shape_list, x_crop,
                                                           h_crop, crop_region1, crop_region2,
                                                           width, obj_confd, each_path)
                result_img_list.append(processed_img_result)

            # To draw image if the draw_output is True
            if draw_output:
                # Create process list in preparation for multithread
                out_process_list = []
                for each_image, each_result, each_path in zip(img_list_c, result_img_list, img_path_c):
                    one_process = [each_result, each_image, each_path, DEBUG_MODE, car_front_shape_list,
                                   crop_region1, crop_region2]
                    out_process_list.append(one_process)

                # Multithread the drawing output of the images
                list(executor.map(draw_image_output, out_process_list))

        # If all images are corrupted, to declare empty variables for the following
        else:
            result_img_list = []
            img_path_c = []

    # Reformat the results
    return_list = []
    for each_result, each_path in zip(result_img_list, img_path_c):
        dict_element = {'img path': f'{each_path}', 'obj': each_result}
        return_list.append(dict_element)

    # Reconstruct list b (npi filtered) from list c
    if fail_npi_index:
        for each_npi_index, each_npi_path, each_npi_status in zip(fail_npi_index, fail_npi_path, fail_npi_status):
            # Insert error 1 message to the corrupted image
            bright_percent = f'{each_npi_status[1]:.2f}'
            dark_percent = f'{each_npi_status[2]:.2f}'
            pix_bright_val = f'{UPLIMIT_BRIGHT}-{LOWLIMIT_BRIGHT}'
            pxi_dark_val = f'{UPLIMIT_DARK}-{LOWLIMIT_DARK}'
            npi_dict = {'img path': f'{each_npi_path}', 'obj': [{'NPI': -1, 'Bright Percent': bright_percent,
                                                                 'Dark Percent': dark_percent,
                                                                 'Bright Value': pix_bright_val,
                                                                 'Dark Value': pxi_dark_val}]}
            return_list.insert(int(each_npi_index), npi_dict)

    # Handling corrupted images
    if corrupt_index:
        for each_corrupt_index, each_corrupt_path in zip(corrupt_index, corrupt_path):
            # Insert error 1 message to the corrupted image
            corrupt_dict = {'img path': f'{each_corrupt_path}', 'obj': [{'Error': -1}]}
            return_list.insert(int(each_corrupt_index), corrupt_dict)

    return return_list
